function purcell = fvc_purcell(r, EMT, fN,U,S,M,Gram,EM,OPTIONS)
%% Computes the radiative decay rate enhancement at each grid-point

%------------dipole decay rate in vacuum as normalization ------------%
AscaH = @(x) mv_symG(x, fN, M.idx, EM.ce);
sigma=zeros(size(M.idx));sigma(1)=1;
gamma0=AscaH(sigma);gamma0=gamma0(1);
%---------------------------------------------------%

% -------------------------------------------------------------------------
% Compute
%         XU = W' * U
% where W = inv(A)
% -------------------------------------------------------------------------

fprintf('\n ------------------------------ \n' )
fprintf('Solve for U of symG \n\n')

XU = zeros(size(U));

% Solve the system for each singular vector
tic
parfor nU = 1:size(U,2)
    fprintf('Solving %3d /%3d \r',nU,size(U,2))
    [XU(:,nU)] = solve_WtransU(U(:,nU),fN,r,EMT,M,OPTIONS);
end
Time_XU = toc;
fprintf('Time_XU      = %dm%ds  \n\n' ,floor(Time_XU/60),int64(mod(Time_XU,60)))

L_S = sqrtm(S);
Q = XU * L_S;
gamma=-sum(Q.*conj(Q),2)*Gram^2;

purcellall=real(gamma/gamma0); %purcell enhance, for each polarizations
sz=length(gamma)/3;
purcell=(purcellall(1:sz)+purcellall(1+sz:2*sz)+purcellall(1+2*sz:3*sz))/3;%average in each polarization

fprintf('\n       --- \n' )
fprintf('\n       max purcell enhance = %1.2f \n', max(purcell));
fprintf('\n       average purcell enhance = %1.2f \n', mean(purcell));
fprintf('\n       min purcell enhance = %1.2f \n', min(purcell));
end